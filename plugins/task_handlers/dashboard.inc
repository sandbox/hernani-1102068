<?php

/**
 * @file
 *
 * This is the task handler plugin to handle attaching a panel to any
 * task that advertises itself as a 'context' type, which all of the
 * basic page tasks provided by page_manager.module do by default.
 *
 * Most of our config is handled by panels page_manager.inc, but we do have
 * to customize it a little bit to match what we're doing with this task.
 */

/**
 * hook_page_manager_task_handlers().
 */
function panels_node_dashboard_dashboard_page_manager_task_handlers() {
  return array(
    'handler type' => 'context',
    'title' => t('Dashboard'),
    'admin summary' =>'panels_node_dashboard_dashboard_admin_summary',
    'render' => 'panels_node_dashboard_dashboard_render',
    'visible' => TRUE,
    'add features' => array(
        'criteria' => t('Selection rules'),
        'context' => t('Contexts'),
    ),
    'add finish' => 'settings',
    'edit forms' => array(
      'criteria' => t('Selection rules'),
      'settings' => t('General'),
      'context' => t('Contexts'),
    ),
    'tab operation' => 'panels_panel_context_tab_operation',
    'operations' => array(
      'settings' => array(
        'title' => t('General'),
        'description' => t('Change general settings for this variant.'),
        'form' => 'panels_node_dashboard_dashboard_edit_settings',
      ),
      'criteria' => array(
        'title' => t('Selection rules'),
        'description' => t('Control the criteria used to decide whether or not this variant is used.'),
        'ajax' => FALSE,
        'form' => array(
          'order' => array(
            'form' => t('Selection rules'),
          ),
          'forms' => array(
            'form' => array(
              'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
              'form id' => 'ctools_context_handler_edit_criteria',
            ),
          ),
        ),
      ),
      'context' => array(
        'title' => t('Contexts'),
        'ajax' => FALSE,
        'description' => t('Add additional context objects to this variant that can be used by the content.'),
        'form' => array(
          'order' => array(
            'form' => t('Context'),
          ),
          'forms' => array(
            'form' => array(
              'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
              'form id' => 'ctools_context_handler_edit_context',
            ),
          ),
        ),
      ),
    ),
    'forms' => array(
      'settings' => array(
        'form id' => 'panels_node_dashboard_dashboard_edit_settings',
      ),
      'context' => array(
        'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
        'form id' => 'ctools_context_handler_edit_context',
      ),
      'criteria' => array(
        'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
        'form id' => 'ctools_context_handler_edit_criteria',
      ),
    ),
    'default conf' => array(
      'autogenerate_title' => FALSE
    ),
  );
}

/**
 * Check selection rules and, if passed, render the contexts.
 *
 * We must first check to ensure the node is of a type we're allowed
 * to render. If not, decline to render it by returning NULL.
 */
function panels_node_dashboard_dashboard_render($handler, &$base_contexts, $args, $test = TRUE) {
  global $user;
  $node = $base_contexts['argument_nid_1']->data;


     
  /* there is a group customization */  
  if($handler->conf['dashboard_settings']=='group' && $node->type=='group' && og_is_group_admin($node)) {
    $dashboards=panels_node_dashboard_get_dashboard_instances($handler->task, $handler->subtask, 'node', $node->nid);
    $dashboard_to_use=array_shift($dashboards);
  
    if(is_null($dashboard_to_use)) {
      $panels_node_dashboard_links[]=l(t('Customize Group Dahsboard'),
              'panels_node_dashboard/customize/'.$handler->task.'/'.$handler->subtask.'/node/'.$node->nid,array('query'=>array('final_destination' => $_GET['q'])));
    }
    
    else {
      $panels_node_dashboard_links[]=l(t('Edit Group Dashboard'),
              'panels_node_dashboard/panel_content/'.$dashboard_to_use->did, array('query'=>array('destination' => $_GET['q'])));

      $panels_node_dashboard_links[]=l(t('Reset Group Dashboard'),
              'panels_node_dashboard/reset_content/'.$handler->task.'/'.$handler->subtask.'/node/'.$node->nid, array('query'=>array('destination' => $_GET['q'])));
  
    }
  }
  else if (user_is_logged_in() && $handler->conf['dashboard_settings']=='user') {
    $dashboards=panels_node_dashboard_get_dashboard_instances($handler->task, $handler->subtask, 'user', $user->uid);
    $dashboard_to_use=array_shift($dashboards);
    
    if (is_null($dashboard_to_use)) 
      $panels_node_dashboard_links[]=l(t('Customize Own Dahsboard'),
              'panels_node_dashboard/customize/'.$handler->task.'/'.$handler->subtask.'/user/'.$user->uid,array('query'=>array('final_destination' => $_GET['q'])));
    else {
      $panels_node_dashboard_links[]=l(t('Edit Own Dashboard'),
              'panels_node_dashboard/panel_content/'.$dashboard_to_use->did,array('query'=>array('destination' => $_GET['q'])));   
         $panels_node_dashboard_links[]=l(t('Reset Own Dashboard'),
               'panels_node_dashboard/reset_content/'.$handler->task.'/'.$handler->subtask.'/user/'.$user->uid, array('query'=>array('destination' => $_GET['q'])));
    
    } 
  }
  
  /* add a new context if so */
  if(!empty($panels_node_dashboard_links))
      $base_contexts['panels_node_dashboard_context']->links=$panels_node_dashboard_links;
  
  /* we have a new display to show, so load it and pass to it the right context */
  if(!is_null($dashboard_to_use)) {
       
       $new_display = panels_load_display($dashboard_to_use->did);      
       $new_display->context=$base_contexts;
       $new_display->args=$args;       
       $output = panels_render_display($new_display);
   
       $info = array(
            'title' => $node->title,
            'content' => $output,
            'no_blocks' => $handler->conf['no_blocks'],
       );
       return $info;
   }
   
      
}

/**
 * Provide a nice little summary of this task handler.
 *
 * TODO: make this nicer for admins to understand.
 */
function panels_node_dashboard_dashboard_admin_summary($task, $subtaskd) {
  $output = t('Using Panels Node Dashboard');
  return $output;
}

/**
 * General settings for the panel
 */
function panels_node_dashboard_dashboard_edit_settings(&$form, &$form_state) {
  $conf = $form_state['handler']->conf;
  $form['conf']['title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['title'],
    '#title' => t('Administrative title'),
    '#description' => t('Administrative title of this variant.'),
  );

  $form['conf']['no_blocks'] = array(
    '#type' => 'checkbox',
    '#default_value' => $conf['no_blocks'],
    '#title' => t('Disable Drupal blocks/regions'),
    '#description' => t('Check this to have the page disable all regions displayed in the theme. Note that some themes support this setting better than others. If in doubt, try with stock themes to see.'),
  );

  $form['conf']['css_id'] = array(
    '#type' => 'textfield',
    '#size' => 35,
    '#default_value' => $conf['css_id'],
    '#title' => t('CSS ID'),
    '#description' => t('The CSS ID to apply to this page'),
  );

  $form['conf']['css'] = array(
    '#type' => 'textarea',
    '#title' => t('CSS code'),
    '#description' => t('Enter well-formed CSS code here; this code will be embedded into the page, and should only be used for minor adjustments; it is usually better to try to put CSS for the page into the theme if possible. This CSS will be filtered for safety so some CSS may not work.'),
    '#default_value' => $conf['css'],
  );
  
  
  $form['conf']['dashboard_settings']=array(
      '#title'=>t('Panels Node Dashboard for'),
      '#type'=>'radios',
      '#required'=>true,
      '#default_value' => $conf['dashboard_settings'],
      '#options'=>array(
        'user' => t('User Dashboard'),
        'group' => t('Group Dashboard')
       ),
   );
}


function panels_node_dashboard_dashboard_edit_settings_submit(&$form, &$form_state) {
  $form_state['handler']->conf['title'] = $form_state['values']['title'];
  $form_state['handler']->conf['no_blocks'] = $form_state['values']['no_blocks'];
  $form_state['handler']->conf['css_id'] = $form_state['values']['css_id'];
  $form_state['handler']->conf['css'] = $form_state['values']['css'];
  $form_state['handler']->conf['dashboard_settings'] = $form_state['values']['dashboard_settings'];
}
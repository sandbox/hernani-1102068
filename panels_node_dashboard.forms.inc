<?php 

   /* form to confirm  customize panel */
   function panels_node_dashboard_customize_dashboard_form ($form_state, $task, $subtask, $type, $id) {
     
      $form['task'] = array(
        '#type' => 'value',
        '#value' => $task,
      );

      $form['subtask'] = array(
        '#type' => 'value',
        '#value' => $subtask=='null' ? '' : $subtask,
      );
      
      $form['final_destination'] = array(
        '#type' => 'value',
        '#value' => $_GET['final_destination'],
      );
      
      $form['type'] = array(
        '#type' => 'value',
        '#value' => $type,
      );
      
      $form['id'] = array(
        '#type' => 'value',
        '#value' => $id,
      );
  
     return confirm_form($form,
        t('Are you sure you want to customize this dashboard?', array('%title' => $node->title)),
        isset($_GET['destination']) ? $_GET['destination'] : 'node/'. $node->nid,
        t('By confirming you can have a personalized dashboard, changing the dashboard layout, adding new widgets or configuring existing ones!'),
        t('Yes'),
        t('Cancel')
      );

   }
   
   
   /* Execute dashboard customization */
  function panels_node_dashboard_customize_dashboard_form_submit($form_id, &$form_state) {
    
    if ($form_state['values']['confirm']) {
      $did=panels_node_dashboard_customize_dashboard($form_state['values']['task'], $form_state['values']['subtask'], $form_state['values']['type'], $form_state['values']['id']);
    }
    
    drupal_goto("panels_node_dashboard/panel_content/".$did,array('destination' => $form_state['values']['final_destination']));
  }
     
  function panels_node_dashboard_reset_content_form($form_state, $task, $subtask, $entity_type, $entity_id) {

    $form['task']=array(
        '#type' => 'value',
        '#value' => $task
    );
    
    $form['subtask']=array(
        '#type' => 'value',
        '#value' => $subtask
    );
    
    $form['final_destination'] = array(
        '#type' => 'value',
        '#value' => $_GET['final_destination'],
    );
    
    $form['entity_type']=array(
        '#type' => 'value',
        '#value' => $entity_type
    );

    $form['entity_id']=array(
        '#type' => 'value',
        '#value' => $entity_id
    );
    
    return confirm_form($form,
        t('Are you sure you want to reset this dashboard to the default state?'),
        isset($_GET['destination']) ? $_GET['destination'] : 'node/'. $node->nid,
        t('By confirming you will have the default content and layout appearing again in your dashboard.'),
        t('Yes'),
        t('Cancel')
      );
  }
  
  function panels_node_dashboard_reset_content_form_submit($form_id, &$form_state) {
    if ($form_state['values']['confirm']) {
      panels_node_dashboard_delete_dashboard_instance ($form_state['values']['task'], $form_state['values']['subtask'], $form_state['values']['entity_type'], $form_state['values']['entity_id']);
    }
   
  }
   
?>